const model = require('../models/contract')
const joi = require('joi')

function getAll(req, res) {
    const schema = joi.object().keys({
        page: joi.number().integer(),
        perPage: joi.number().integer(),
    })

    const validation = schema.validate(req.query)

    if (validation.error != null) {
        res.status(400).json({
            errors: validation.error.details.map((e) => e.message),
        })
        return
    }

    let { page, perPage } = req.query
    page = (parseInt(page) || 1) - 1
    perPage = parseInt(perPage) || 10

    const starts = page * perPage
    const ends = starts + perPage
    const contracts = model.getContracts(starts, ends)

    res.json(contracts)
}
function getOne(req, res) {
    const id = req.params.id
    const contract = model.getContract(id)
    if (!contract) {
        res.status(404).json({
            errors: ['Contract with given ID not found'],
        })
    } else {
        res.json(contract)
    }
}

function updateOne(req, res) {
    if (!model.validateContract(req.body)) {
        res.status(400).json({
            errors: validation.error.details.map((e) => e.message),
        })
        return
    }

    const id = req.params.id
    const current = model.getContract(id)
    if (!current) {
        res.status(404).json({
            errors: ['Contract with given ID not found'],
        })
        return
    }
    const newContract = model.updateContract(id, req.body)
    res.status(200).json(newContract)
}
module.exports = {
    getAll,
    getOne,
    updateOne,
}
