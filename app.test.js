const request = require('supertest')
const app = require('./app')

const expectedContractModel = {
    contract: expect.objectContaining({
        id: expect.any(Number),
        total: expect.any(Number),
        currency_code: expect.any(String),
        payment_type: expect.stringMatching(/full|partial/),
        payment_timing: expect.stringMatching(/begin|end/),
        payment_period: expect.stringMatching(/yearly|monthly|weekly|quarterly/),
        payment_amount: expect.any(Number),
        payment_currency_code: expect.any(String),
        auto_renew: expect.any(Boolean),
        cancel_early: expect.any(Boolean),
        description: expect.any(String),
        endDate: expect.any(String),
        beginDate: expect.any(String),
        duration: expect.any(Number),
        duration_type: expect.stringMatching(/years|months|days/),
        status: expect.stringMatching(/draft|not\-active|active|expired|terminated/),
    }),
}

describe('Contract Tests', () => {
    it('GET /contracts ---> array of 10 contracts', () => {
        return request(app)
            .get('/contracts')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                const contracts = response.body
                expect(contracts).toEqual(expect.arrayContaining([expect.objectContaining(expectedContractModel)]))
                expect(contracts).toHaveLength(10)
            })
    })
    it('GET /contracts?page=3 ---> array of the third page of contracts', () => {
        return request(app)
            .get('/contracts?page=3')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                const contracts = response.body
                expect(contracts).toEqual(expect.arrayContaining([expect.objectContaining(expectedContractModel)]))
                expect(contracts).toHaveLength(5)
            })
    })
    it('GET /contracts?perPage=20 ---> array of 20 contracts', () => {
        return request(app)
            .get('/contracts?perPage=20')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                const contracts = response.body
                expect(contracts).toEqual(expect.arrayContaining([expect.objectContaining(expectedContractModel)]))
                expect(contracts).toHaveLength(20)
            })
    })
    it('GET /contracts?perPage=20&page=2 ---> array of the second page of contracts with 20 items', () => {
        return request(app)
            .get('/contracts?perPage=20&page=2')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                const contracts = response.body
                expect(contracts).toEqual(expect.arrayContaining([expect.objectContaining(expectedContractModel)]))
                expect(contracts).toHaveLength(5)
            })
    })
    it('GET /contracts?perPage=20&page=3 ---> array of the third page of contracts with 0 items', () => {
        return request(app)
            .get('/contracts?perPage=20&page=3')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                const contracts = response.body
                expect(contracts).toEqual(expect.arrayContaining([]))
                expect(contracts).toHaveLength(0)
            })
    })
    it('GET /contracts?perPage=abc&page=2 ---> array of the second page of contracts with wrong param value', () => {
        return request(app).get('/contracts?perPage=abc&page=3').expect(400)
    })
    it('GET /contracts?perPage=10&page=abc ---> array of contracts with wrong values values for pagination', () => {
        return request(app).get('/contracts?perPage=10&page=abc').expect(400)
    })
    it('GET /contracts/:id ---> contract by ID', () => {
        return request(app)
            .get('/contracts/1')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                const res = response.body
                expect(res).toEqual(expect.objectContaining(expectedContractModel))
                expect(res.contract.id).toBe(1)
            })
    })
    it('GET /contracts/:id ---> 404 if not found', () => {
        return request(app).get('/contracts/9876543215468').expect(404)
    })
    it('PATCH /contracts/:id ---> updated contract with new values', () => {
        return request(app)
            .patch('/contracts/1')
            .send({
                description: 'Wibbly wobbly, timey wimey',
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                const res = response.body
                expect(res).toEqual(expect.objectContaining(expectedContractModel))
                expect(res.contract.description).toBe('Wibbly wobbly, timey wimey')
            })
    })
    it('PATCH /contracts/:id ---> error 404 when trying to update an unknown contract ', () => {
        return request(app)
            .patch('/contracts/1123123123')
            .send({
                description: 'Wibbly wobbly, timey wimey',
            })
            .expect(404)
    })
    it('PATCH /contracts/:id ---> error 400 when trying to update protected values', () => {
        return request(app)
            .patch('/contracts/1')
            .send({
                id: 5,
            })
            .expect(400)
    })
    it('PATCH /contracts/:id ---> error 400 when trying to update payment period with unknown values', () => {
        return request(app)
            .patch('/contracts/1')
            .send({
                payment_period: 'testing',
            })
            .expect(400)
    })
})
