const Joi = require('joi')

const schema = Joi.object().keys({
    total: Joi.number(),
    currency_code: Joi.string(),
    payment_type: Joi.string().valid('full', 'partial'),
    payment_timing: Joi.string().valid('begin', 'end'),
    payment_period: Joi.string().valid('yearly', 'monthly', 'weekly', 'quarterly'),
    payment_amount: Joi.number(),
    payment_currency_code: Joi.string(),
    auto_renew: Joi.boolean(),
    cancel_early: Joi.boolean(),
    description: Joi.string(),
    endDate: Joi.date(),
    beginDate: Joi.date(),
    duration: Joi.number().integer(),
    duration_type: Joi.string().valid('years', 'months', 'days'),
    status: Joi.string().valid('draft', 'not-active', 'active', 'expired', 'terminated'),
})

function db() {
    let data = require('./raw.json')

    function getContract(id) {
        return data.find((c) => c.contract.id == id)
    }

    function getContracts(startsAt, endsAt) {
        return data.slice(startsAt, endsAt)
    }

    function updateContract(id, newData) {
        const idx = data.findIndex((c) => c.contract.id == id)
        const current = data[idx]
        const updated = {
            ...current,
            contract: { ...current.contract, ...newData, updatedAt: new Date().toISOString },
        }
        data[idx] = updated

        return updated
    }

    function validateContract(contract) {
        const result = schema.validate(contract)
        if (result.error) {
            return false
        }
        return true
    }

    return {
        getContract,
        getContracts,
        updateContract,
        validateContract,
    }
}

module.exports = db()
