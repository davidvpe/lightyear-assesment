const express = require('express')

const contractsController = require('../controllers/contract')

const router = express.Router()

router.get('/:id', contractsController.getOne)
router.get('/', contractsController.getAll)
router.patch('/:id', contractsController.updateOne)

module.exports = router
