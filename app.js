const express = require('express')
const contractRouter = require('./routes/contract')

const app = express()

app.use(express.json())

app.use('/contracts', contractRouter)

module.exports = app
